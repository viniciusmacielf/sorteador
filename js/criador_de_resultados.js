function alternar_display_fs_grupos() {
    var elem_radios = document.getElementsByName("radio_tipo");
    var elem_fs_grupos = document.getElementById("fs_grupos");
    if(elem_radios[0].checked) {
        elem_fs_grupos.style = "display: block";
    }
    else {
        elem_fs_grupos.style = "display: none";   
    }
}

function limpar (limpar_tudo) {
    var elem_dicas = document.getElementById("dicas");
    var elem_resultados = document.getElementById("resultados");
    elem_resultados.innerHTML = "";
    if(limpar_tudo) {
        var elem_tipo = document.getElementsByName("radio_tipo");
        var elem_n_itens = document.getElementById("n_itens");
        elem_dicas.style = "display:block";
        elem_n_itens.value = elem_n_itens.getAttribute("min");
        elem_tipo[0].checked = true;
        elem_tipo[1].checked = false;
        alternar_display_fs_grupos();
        visibilidade_botao_download(false);
    }
    else {
        elem_dicas.style = "display:none";
    }
}

/*
    Funções relativas ao sorteio
*/

function visibilidade_botao_download(visivel) {
    var elem_btnd_download = document.getElementById("btnd_download");
    if(visivel)
        elem_btnd_download.style = "display: block";
    else
        elem_btnd_download.style = "display: none";
}

function embaralhar(itens) {
    for(var i = 0; i < itens.length; i++) {
        var pos = Math.floor(Math.random() * itens.length);
        [itens[i], itens[pos]] = [itens[pos], itens[i]];
    }  
}

function sortear() {
    var elem_textarea_itens = document.getElementById("textarea_itens");
    var elem_tipo = document.getElementsByName("radio_tipo");
    var elem_n_itens = document.getElementById("n_itens");

    var itens_str = elem_textarea_itens.value;
    var selecao_tipo_1 = elem_tipo[0].checked;
    var n_itens_str = elem_n_itens.value;

    // verificando se o campo de itens nao possui apenas caracteres vazios
    // ou o campo nao esta vazio.
    if(itens_str.trim().length != 0) {
        limpar(false);
        var itens = itens_str.split("\n");
        // filtrar todos itens vazios
        itens = itens.filter((item) => item.trim() != '');
        embaralhar(itens);
        if(selecao_tipo_1) {
            var n_itens = parseInt(n_itens_str);
            if(n_itens < 1) {
                n_itens = elem_n_itens.getAttribute("min");
                elem_n_itens.value = n_itens;
            }
            sorteio_tipo_1(itens, n_itens);
        }
        else {
            sorteio_tipo_2(itens);
        }
    }

/*
    Funções de sorteio por tipo
*/

function sorteio_tipo_1(itens, n_itens) {
    var grupo = [];
    var id = 1;
    for(var i = 0; i < itens.length; i++) {
        grupo.push(itens[i]);
        if(grupo.length == n_itens) {
            cria_tabela("Grupo "+id, grupo);
            grupo = [];
            id++;
        }
    }
    if(grupo.length > 0) {
        cria_tabela("Grupo "+id, grupo);
    }
    visibilidade_botao_download(true);
}
function sorteio_tipo_2(itens) {
    cria_tabela("Sorteio", itens);
    visibilidade_botao_download(true);
}

/*
    As funções abaixo são as que criam a tabela.
*/

function cria_tabela(cabecalho_rotulo, itens) {
    var elem_resultados = document.getElementById("resultados");
    var table = cria_estrutura_da_tabela(elem_resultados);
    cria_cabecalho_na_tabela(table, cabecalho_rotulo);
    for(var i = 0; i < itens.length; i++) {
        cria_linha_na_tabela(table, itens[i], i+1);
    }
}

function cria_estrutura_da_tabela(elem_root) {
    var table = document.createElement("table");
    table.appendChild(document.createElement("thead"));
    table.appendChild(document.createElement("tbody"));
    elem_root.appendChild(table);
    return table;
}

function cria_cabecalho_na_tabela(table, cabecalho_rotulo) {
    var thead = table.getElementsByTagName('thead')[0];
    var tr = document.createElement('tr');
    var th = document.createElement('th');
    th.setAttribute("colspan", 2);
    th.innerHTML = cabecalho_rotulo;
    tr.appendChild(th);
    thead.appendChild(tr);
}

function cria_linha_na_tabela(table, item, i) {
    var tbody = table.getElementsByTagName('tbody')[0];
    var tr = document.createElement('tr');
    var tdCol1 = document.createElement('td');
    var tdCol2 = document.createElement('td');
    tdCol1.setAttribute("class", "col1");
    tdCol2.setAttribute("class", "col2");
    tdCol1.innerHTML = i;
    tdCol2.innerHTML = item;
    tr.appendChild(tdCol1);
    tr.appendChild(tdCol2);
    tbody.appendChild(tr);
}

}