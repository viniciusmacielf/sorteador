function criar_pagina() {
    var style = `<style type="text/css">
table {
    width: 95%;
    margin: 30px auto;
    border-collapse: collapse;
}

td, th {
    border: 1px solid rgb(221, 221, 221); 
    padding: 8px;
}

.col1 {
    width: 5%;
}

.col2 {
    width: 95%;
}

th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: center;
    background-color: rgb(76, 194, 72);
    color: white;
}

tr:nth-child(even) {
    background-color: rgb(242, 242, 242);
}

tr:hover {
    background-color: rgb(221, 221, 221);
}
</style>`;

    var body = document.getElementById("resultados").innerHTML;

    return "<html><head>"+style+"</head><body>"+body+"</body></html>";
}

function download() {
    var a = document.createElement("a");
    a.download = "exportar_resultados.html";
    a.href = "data:text/html," + criar_pagina();
    a.click();
}